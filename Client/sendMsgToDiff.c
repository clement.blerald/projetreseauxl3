#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include "sendMsgToDiff.h"
#include "setSocket.h"

//send message wrote by user to diffuseur 
int sendToDiff(char *identifiant, char *addr_tcp,int port_tcp, char *msg){
 
  //create socket
  int sock;
  if(setSockTCP(&sock,port_tcp,addr_tcp) == EXIT_FAILURE){
    perror("creation socket TCP");
    return EXIT_FAILURE;
  }
    
  char toSend[157];
  memset(toSend,0,157);

  char msgBuff[141];
  strcpy(msgBuff,msg);

  if(strlen(msgBuff) < 140)
    memset(msgBuff + strlen(msgBuff),'#', 140 - strlen(msgBuff));

  msgBuff[140] = '\0';
  sprintf(toSend,"MESS %s %s\r\n",identifiant,msgBuff);
  
  //send msg
  if(send(sock,toSend,strlen(toSend),0) == -1){
      perror("send");
      return EXIT_FAILURE;
  }

  //receiv ACKM check
  char buff[7];
  int size_rec=recv(sock,buff,6,0);
  buff[size_rec]='\0';

  if(strcmp(buff,"ACKM\r\n") != 0){
    perror("On n'as pas reçu ACKM");
    printf("buff = |%s|\n",buff);
    return EXIT_FAILURE;
  }
    
  printf("Bien reçu ACKM\n");
  fflush(stdout);

  close(sock);
  return EXIT_SUCCESS;
}