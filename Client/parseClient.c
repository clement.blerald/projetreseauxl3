#include "parseClient.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

//parse config file
int parse(char *id, char *addr_multi, int *port_multi, char *addr_tcp, int *port_diff, int *port_gest){   
    
    //open
    int fd = open("../config-client.txt",O_RDONLY);
    if(fd == -1){
        perror("file not found");
        return EXIT_FAILURE;
    }

    //read
    char buff[1024];
    memset(buff,0,1024);

    int r = read(fd,buff,1024);
    if(r < 1){
        perror("erreur lecture");
        return EXIT_FAILURE;
    }

    //get all infos with strtok
    char tmp_id[9];
    memset(tmp_id,0,9);
    sprintf(tmp_id,"%s",strtok(buff,"\n"));
  
    if(strlen(tmp_id) < 8){
       char diese[8 - strlen(tmp_id) + 1];
       memset(diese,'#',8 - strlen(tmp_id));
       diese[8 - strlen(tmp_id)] = '\0';
       sprintf(id,"%s%s",tmp_id,diese); 
    }else
       sprintf(id,"%s",tmp_id);
    
    sprintf(addr_multi,"%s",strtok(NULL,"\n"));
    *port_multi = atoi(strtok(NULL,"\n"));
    sprintf(addr_tcp,"%s",strtok(NULL,"\n"));
    *port_diff = atoi(strtok(NULL,"\n"));
    *port_gest = atoi(strtok(NULL,"\n"));
    
    
    return EXIT_SUCCESS;
}