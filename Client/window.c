#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include "receiveMulti.h"
#include "setSocket.h"
#include "parseClient.h"
#define SIZE 512
#define ID 8
#define PORT 15

//Main program to receiv UDP messages
int main(int argc, char ** argv){

    char id[ID + 1];
    char addr_multi[PORT + 1];
    char addr_tcp[PORT + 1];
    int port_multi, port_diff, port_gest;
    
    //parse config file
    if(parse(id,addr_multi,&port_multi,addr_tcp,&port_diff,&port_gest) == EXIT_FAILURE){
        perror("fichier config");
        return EXIT_FAILURE;
    }
    
    //set socket UDP
    int sockMulti;
    if(setsockmulti(&sockMulti,port_multi,addr_multi) == EXIT_FAILURE){
        perror("creation sockMulti");
        return EXIT_FAILURE;
    }

    //lauch program
    if(receivDiff(sockMulti) == EXIT_FAILURE){
        perror("receivDiff");
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;    
}