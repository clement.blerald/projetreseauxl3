#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "sendFile.h"
#include "sendMsgToDiff.h"

//send a file to diff
int sendFile(char *identifiant,char *path, char *addr_tcp, int port_tcp){
    
    //open file
    int fd = open(path,O_RDONLY);
    if(fd == -1){
        perror("error open");
        return EXIT_SUCCESS;
    }

    //get file size
    off_t size = lseek(fd,0,SEEK_END);
    lseek(fd,0,SEEK_SET);

    char buff[size+1];
    memset(buff,0,size+1);

    //read
    int rec = read(fd,buff,size);
    close(fd);
    
    if(rec != size){
        perror("error read");
        return EXIT_FAILURE;
    }

    buff[size]= '\0';
    
    int pos = 0;

    //send msg cut by 140 char
    while (pos<size){
        char toSend[141];
        strncpy(toSend,buff+pos,140);
        sendToDiff(identifiant,addr_tcp,port_tcp,toSend);
        pos += 140;
    }
    
    return EXIT_SUCCESS;
}