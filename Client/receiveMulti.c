#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include "receiveMulti.h"
#include <ncurses.h>
#include "parseClient.h"

//program to get msg send multicast
int receivDiff(int sock){
    char tampon[162];
    memset(tampon,0,162);

    //create window
    initscr();    
    scrollok(stdscr,TRUE);

    while(1){
        //receiv msg
        int rec=recv(sock,tampon,161,0);
        
        if(rec != 161){
            printf("rec = %i \nbuff = %s",rec,tampon);
            fflush(stdout);
            perror("probleme receiv");
            return EXIT_FAILURE;
        }

        //eliminate # and display msg
        tampon[161]='\0';
        char *diese = strchr(tampon + 20,'#');
        if(diese != NULL){
            int fin = strlen(diese);
            char toDisplay[161 - fin - 4];
            snprintf(toDisplay,161 - fin - 4,"%s",tampon + 5); 
            printw("Received : %s\n\n",toDisplay); 
        }else
            printw("Received : %s\n\n",tampon);
        fflush(stdout);
        refresh();
    }

    getch();
    endwin();
    return EXIT_SUCCESS;
}

