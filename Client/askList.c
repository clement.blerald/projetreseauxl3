#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include "askList.h"
#include "setSocket.h"

//ask list of diffuseur to gestionnaire
int askList(char *addr_tcp, int port_tcp){
    
    //create socket
    int sock;
    if(setSockTCP(&sock,port_tcp,addr_tcp) == EXIT_FAILURE){
        perror("creation socket TCP");
        return EXIT_FAILURE;
    }
    
    //send
    char *mess = "LIST\r\n";
    send(sock,mess,strlen(mess),0);

    //recv
    char buff[10];
    memset(buff,0,10);
    int rec = recv(sock,buff,9,0);
    buff[rec] = '\0'; 

    int nbMsg = 0;
    
    //check message is correct
    if(strncmp(buff,"LINB",4) == 0 && strcmp(buff + 7,"\r\n") ==0 && (nbMsg = (atoi(buff+5))) != 0){
        printf("Ce gestionnaire à enregistré %i diffuseur(s)\n\n",nbMsg);
        fflush(stdout);

        char data[58];
        memset(data,0,58);
        
        //receiv diffeurs infos 1 by 1
        for(int i = 0; i < nbMsg; i++){
            
            if(recv(sock,data,57,0) != 57 || strncmp(data,"ITEM",4) != 0){
                perror("msg size");
                printf("size : %li",strlen(data));
                return EXIT_FAILURE;
            }

            char id[9];
            id[8] = '\0';
            char ip1[16];
            ip1[15] = '\0';
            char port1[5];
            port1[4] = '\0';
            char ip2[16];
            ip2[15] = '\0';
            char port2[5];
            port2[4] = '\0';

            memcpy(id,data + 5,8);
            memcpy(ip1,data + 14,15);
            memcpy(port1,data + 30,4);
            memcpy(ip2,data + 35,15);
            memcpy(port2,data + 51,4); 

            //display messages
            printf("id : %s\nip1 : %s\nport1 : %s\nip2 : %s\nport2 : %s\n\n\n",id,ip1,port1,ip2,port2);
            fflush(stdout);
        }
    }else{
        printf("Ce gestionnaire n'as pas enregistré de diffuseurs ou à renvoyé autre chose que leur nombre\n");
    }
    close(sock);
    return EXIT_SUCCESS;
}