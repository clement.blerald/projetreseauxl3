#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include "askLastMsg.h"
#include "setSocket.h"

//ask last n messages to diffuseur
int askMsg(char *id,char *addr_tcp, int port_tcp,int n){

    //create socket
    int sock;
    if(setSockTCP(&sock,port_tcp,addr_tcp) == EXIT_FAILURE){
        perror("creation socket TCP");
        return EXIT_FAILURE;
    }

    //fill id with #
    if(strlen(id) < 8)
        memset(id + strlen(id),'#', 8 - strlen(id));
  
    id[8] = '\0';

    char nbS[5];
    memset(nbS,0,5);
    sprintf(nbS,"%03d",n); 
    
    //send
    char msg[12];
    memset(msg,0,12);
    sprintf(msg,"LAST %s\r\n",nbS);


    msg[strlen(msg)] = '\0';

    send(sock,msg,strlen(msg),0);

    //receiv
    char buff[162];
    memset(buff,0,162);
    int rec = recv(sock,buff,161,0);

    //display all msg
    while(strcmp(buff,"ENDM\r\n") != 0){
        if(rec != 161){
            buff[161] = '\0';
            perror("length message != 161\n");
            printf("taille buff : %li\n",strlen(buff));
            printf(strcmp("\r\n",buff+160)==0 ? "oui\n" : "non\n");
            printf("|%s|",buff);
            fflush(stdout);
            return EXIT_FAILURE;
        }

        buff[161] = '\0';

        //eliminates # and display
        char *diese = strchr(buff + 20,'#');
        if(diese != NULL){
            int fin = strlen(diese);
            char toDisplay[162 - fin];
            memset(toDisplay,0,162 - fin);
            snprintf(toDisplay,162 - fin,"%s",buff);
            printf("%s\n\n",toDisplay+5); 
        }else
            printf("%s\n",buff+5);
          
            
        fflush(stdout);
        memset(buff,0,162);
        
        //recv
        rec = recv(sock,buff,161,0);
    }

    return EXIT_SUCCESS;
}