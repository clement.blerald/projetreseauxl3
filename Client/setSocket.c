#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include "setSocket.h"

//set socket UDP to listen to diffuseur messages
int setsockmulti(int *sock, int port_multi, char *addr_multi){
    *sock=socket(PF_INET,SOCK_DGRAM,0);
    
    //To re use the port
    int ok=1;
    int r=setsockopt(*sock,SOL_SOCKET,SO_REUSEPORT,&ok,sizeof(ok));

    if(r == -1){
        perror("setsockopt 1");
        return EXIT_FAILURE;
    }

    //To bind the port
    struct sockaddr_in address_sock;
    address_sock.sin_family=AF_INET;
    address_sock.sin_port=htons(port_multi);
    address_sock.sin_addr.s_addr=htonl(INADDR_ANY);

    r=bind(*sock,(struct sockaddr *)&address_sock,sizeof(struct sockaddr_in));

    if(r == -1){
        perror("bind");
        return EXIT_FAILURE;
    }

    //To subscribe to a diffuser
    struct ip_mreq mreq;
    mreq.imr_multiaddr.s_addr=inet_addr(addr_multi);
    mreq.imr_interface.s_addr=htonl(INADDR_ANY);

    r=setsockopt(*sock,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq));

    if(r == -1){
        perror("setsockopt 2");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

//set TCP socket with either diff or gestionnaire
int setSockTCP(int *sock,int numPort, char *addr){
  
  struct sockaddr_in adress_sock;
  adress_sock.sin_family = AF_INET;
  adress_sock.sin_port = htons(numPort);
  inet_aton(addr,&adress_sock.sin_addr);
  
  //create socket
  *sock=socket(PF_INET,SOCK_STREAM,0);
  if(*sock == -1){
      perror("creation socket TCP");
      return EXIT_FAILURE;
  }

  //connect socket
  int r=connect(*sock,(struct sockaddr *)&adress_sock, sizeof(struct sockaddr_in));
  if(r == -1){
      perror("connect socket TCP");
      return EXIT_FAILURE;
  }
  return EXIT_SUCCESS; 
}
