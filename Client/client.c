#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include "parseClient.h"
#include "setSocket.h"
#include "sendMsgToDiff.h"
#include "askLastMsg.h"
#include "askList.h"
#include "sendFile.h"
#define SIZE 512
#define PORT 15
#define ID 8

//MAIN PROGRAM 
int main(int argc, char **argv){
    char id[ID + 1];
    char addr_multi[PORT + 1];
    char addr_tcp[PORT + 1];
    int port_multi, port_diff, port_gest;
 
    //get infos from client config file
    if(parse(id,addr_multi,&port_multi,addr_tcp,&port_diff,&port_gest) == EXIT_FAILURE){
        perror("fichier config mal formaté");
        return EXIT_FAILURE;
    }

    //display config infos
    printf("######################CONFIG#######################\n\n");
    printf("id : %s\naddr_multi : %s\naddr_tcp : %s\nport_multi : %i\nport_diff : %i\nport_gest : %i\n\n",id,addr_multi,addr_tcp,port_multi,port_diff,port_gest);

    char invite[11];
    memset(invite,0,11);
    sprintf(invite,"$%s ",id);

    char *man = "N space int(on 3 bits max) - display last n message\nL - display list of diffusers\nM - send random msg to diff\n\n";

    printf("%s",man);
    printf("###################################################\n\n");
    fflush(stdout);

    char buff[SIZE];

    while(1){
        printf("%s",invite);
        fflush(stdout);

        memset(buff,0,SIZE);
        int rec = read(STDIN_FILENO,buff,SIZE);
        buff[rec-1] = '\0';
        
        if(strncmp(buff,"L",1) == 0 && strlen(buff) == 1){//ask list
            printf("Demande de la liste des diffuseurs au gestionnaire\n");
            printf("addresse : %s\n port : %i\n\n",addr_tcp,port_gest);

            if(askList(addr_tcp,port_gest) == EXIT_FAILURE){
                printf("couldn't ask list");
                return EXIT_FAILURE;
            }
        } else if(strncmp(buff,"M",1) == 0 && strlen(buff) < SIZE){//send msg
            printf("Message envoyé au diffuseur\n");
            printf("addresse : %s\n port : %i\n\n",addr_tcp,port_diff);
            
            if(sendToDiff(id,addr_tcp,port_diff,buff + 2) == EXIT_FAILURE){
                printf("Coudn't send to diff");
                return EXIT_FAILURE;
            } 
        } else if(strncmp(buff,"N",1) == 0 && strlen(buff)  <= 5){//N last msg
            int n = atoi(buff+2);
            printf("Demande des %i derniers messages envoyés au diffuseur\n",n);
            printf("addresse : %s\n port : %i\n\n",addr_tcp,port_diff);
            
            if(askMsg(id,addr_tcp,port_diff,n) == EXIT_FAILURE){
                printf("couldn't ask last messages");
                return EXIT_FAILURE;
            }
        } else if(strncmp(buff,"F",1) == 0 && strlen(buff) < SIZE){//sendFile
            printf("Envoie d'un fichier au diffuseur\n");
            printf("addresse : %s\n port : %i\n\n",addr_tcp,port_diff);

            if(sendFile(id,buff+2,addr_tcp,port_diff) == EXIT_FAILURE){
                printf("Couldn't send file");
                return EXIT_FAILURE;
            }
        } else
            printf("Error : unknow cmd : |%s| try with one of the folowing command : \n\n%s",buff,man);
    }
}