import static java.lang.Thread.sleep;
import java.util.LinkedList;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
public class GestionnaireThread implements Runnable {
    LinkedList<Diffuseur> abonne;
    Socket socket;

    public GestionnaireThread(Socket socket,LinkedList<Diffuseur> abonne){
        this.abonne = abonne;
        this.socket = socket;
    }

    public void run() {
        Diffuseur diff = new Diffuseur("","","","","");
        try{
            int maxAbonne = 3;
            BufferedReader br=new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter pw=new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            String mess=br.readLine();
            System.out.println("Message recu :"+mess);

            if(mess.substring(0,4).equals("REGI") ){
                diff = new Diffuseur(mess.substring(5,13),mess.substring(14,29), mess.substring(30,34), mess.substring(35,50), mess.substring(51,55) );
                if(!abonne.contains(diff) && diff.estBienFormater()  && abonne.size() < maxAbonne){
                    abonne.add(diff);
                    pw.print("REOK\r\n");
                    pw.flush();
                        
                    while(true){
                        sleep(5000);
                        pw.print("RUOK\r\n");
                        pw.flush();
                        mess=br.readLine();
                        System.out.println("Message recu :"+mess);
                       
                        if(!mess.equals("IMOK") || mess == null){
                           // System.out.println(mess);
                            //System.out.println(abonne.size());
                            for (int i = 0;i<abonne.size(); i++){
                                if(diff.equals(abonne.get(i))){
                                    abonne.remove(i);
                                }
                            }
                            abonne.remove(diff);
                            System.out.println(abonne.size());
                            br.close();
                            pw.close();
                            socket.close();
                        }
                    }
                }else{
                    pw.print("RENO\r\n");
                    pw.flush();
                    br.close();
                    pw.close();
                    socket.close();
                }

            }
            if(mess.equals("LIST")){
                int numDiff = abonne.size();
                String numDiffStr = "";
                if (numDiff < 10) numDiffStr = "0"+numDiff;
                pw.print("LINB "+numDiffStr+"\r\n");
                pw.flush();
                
        
                for(int i=0; i<abonne.size(); i++){
                    pw.print("ITEM "+abonne.get(i).toString());
                    pw.flush();
                }
            }

        }catch(Exception e){
            System.out.println("Erreur procedure d'enregistrement d'un diffuseur, désolé client nous ne pouvons pas vous donnez plus d'information !");
            for (int i = 0;i<abonne.size(); i++){
                if((diff.toString()).equals(abonne.get(i).toString())){
                    abonne.remove(i);
                }
            }
        }
    }

}
