

public class Diffuseur{
    private String ident;
    private String ip1;
    private String port1;
    private String ip2;
    private String port2;

    public Diffuseur(String id, String ip1, String port1, String ip2, String port2){
        this.ident = id;
        this.ip1=ip1;
        this.port1=port1;
        this.ip2=ip2;
        this.port2=port2;
    }

    public boolean estBienFormater(){
        return (ident.length() == 8 && ip1.length() == 15 && port1.length() == 4 && ip2.length() == 15 && port2.length() == 4 );
    }

    public String getIdent(){
        return this.ident;
    }

    public String getIp1(){
        return this.ip1;
    }

    public String getPort1(){
        return this.port1;
    }

    public String getIp2(){
        return ip2;
    }

    public String getPort2(){
        return port2;
    }
    public String toString(){
        String s = "";
        if(estBienFormater()){
            s = ident+" "+ip1+" "+port1+" "+ip2+" "+port2+"\r\n";
        }
        return s;
    }
}
