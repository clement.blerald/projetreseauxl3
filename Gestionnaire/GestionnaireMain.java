import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import static java.lang.Thread.sleep;
public class GestionnaireMain{
    public static void main(String[] args){
        try{
            ServerSocket serveur=new ServerSocket(4141);
            LinkedList<Diffuseur> abonne = new LinkedList<Diffuseur>();
            System.out.print("Gestionnaire de diffuseur Lancé : ");
            byte[] ip = new byte[]{127,0,0,1};
            System.out.println(InetAddress.getByAddress("Localhost", ip));
            
            while(true){
                Socket socket=serveur.accept();
                GestionnaireThread serv = new GestionnaireThread(socket,abonne);
                Thread t = new Thread(serv);
                t.start();
            }
        }
        catch(Exception e){
            System.out.println("Port indisponible veuillez utilisez un autre s'il vous plait !");
        }
    }
}
