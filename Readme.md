# Projet de Programmation Réseaux

## Membres du groupe 24

- Blérald Clément : 71800092
- BADJI Sidy : 71702458
- Marin Christian : 71703043

## Intructions

- Ordre : d'abord lancer le **Gestionnaire**, puis le **diffuseur**, puis le **client**

### Pour le Client

- **IMPORTANT** :
    - les ports utilisés par le client sont à renseigner dans le fichier client-config.txt se trouvant à la racine (l'ordre des informations à remplir est le suivant) :
        - nom du client (0-8 char)
        - adresse de multi-diffusion du diffuseur
        - port de multi-diffusion du diffuseur
        - adresse du diffuseur
        - port du diffuseur pour le TCP
        - port du Gestionnaire  
- Remarques :
    - le diffuseur et le Gestionnaire doivent se situer sur la **même** machine
    - Le client doit disposer de *deux terminaux* pour fonctionner

- ```> ./clientConsole.sh``` : lance le client. Depuis cette interface le client peut communiquer avec le gestionnaire et le diffuseur

- ```> ./clientUDP.sh``` : ouvre une fenêtre où s'affichera les messages diffusés par le diffuseur

- ```> ./clientClean.sh``` : permet de nettoyer le répertoire du client

## Extension(s)

- Client : L'extension "send File" permet d'envoyer un fichier au diffuseur, dans la console du client il suffit d'écrire :<br> ```> F chemin_vers_fichier ``` (En somme Cette classe est conçut pour lancer le main.rappelé dans le manuel affiché au début du terminal)


### Pour le gestionnaire

- **Compilation**
    - Pour la compilation du gestionnaire nous avons un fichier executable ./gesrun.sh qui compile les classes java à l'aide d'un makefile conçu pour et un fichier ./gesclean.sh pour supprimer les fichier.class qui joue le role de make clean :
    - ```> ./gesrun.sh```
    - ```> ./gesclean.sh```

  - Notre gestionnaire de diffuseur a été implémenté avec trois classe en java que sont :
  -
  - **Diffuseur**
    - Une classe conçu pour stocker les informations du diffuseur ie c'est un objet ayant quatre instances:

        - String ident = Identifiant du diffuseur
        - String ip1, ip2 = Les adresses ip du diffuseur
        - String port1, port 2 = Les ports du diffuseur

        - Il y'a aussi deux fonctions dans cette classe:

            - estBienFormater : Cette fonction gère le bourage des differentes instances c'est regarde si les informations du diffuseur sont dans le bon format avec    les bon octets.
            - toString() : Cette fonction gère l'affichage des information en respectant le bourage(formatage).
            - Les getteur permettant de recuperer les instance en dehors de la classe à cause de l'encapsulation
            String ident = Identifiant du diffuseur
            String ip1, ip2 = Les adresses ip du diffuseur
            String port1, port 2 = Les ports du diffuseur
            Il y'a aussi deux fonctions dans cette classe:

            estBienFormater : Cette fonction gère le bourage des differentes instances c'est regarde si les informations du diffuseur sont dans le bon format avec    les bon octets.
            toString() : Cette fonction gère l'affichage des information en respectant le bourage

  - **GestionnaireThread**
    - Cette classe gère tout ce qui est traitement des informations avec des Thread en suivant me protocole demandé :
        - L'enregistrement d'un diffuseur.
        - Regarde si les diffuseur sont actif sinon les retire de la liste de diffuseur et cela est fait  avec un temps limite grace à un Thread qui tourne forever.
        - Envoie la liste de diffuseur à la demande d'un client avec le bon format

  - **GestionnaireMain**
    - Cette classe lance les differente Thread et gère la concurrence et nous sert de classe principale(main).

### Pour le Diffuseur

- **Compilation**
    - Pour la compilation du gestionnaire nous avons un fichier executable ./gesrun.sh qui compile les classes java à l'aide d'un makefile conçu pour :
    - ```> ./diffrun.sh```

  - **Data**
    -  Il s'agit d'une classe qui construit un objet contenant toutes les informations nécessaires(les différents ip et les ports) ainsi que les messages en multidiffusion.

  - **Main**
    - Il s'agit de la classe qui contient le main
    - Elle contient différentes fonctions afin de formater les ip, les ports et les messages.
    - Elle créer 3 threads qui fonctionnent en simultané : la multidiffusion, la communication avec les clients et la communication avec le gestionnaire.

  - **Multicast**
    - Cette classe lance le multicast. Elle contient les Data ainsi que le nombre de message envoyer et si elle a fini de diffuser.
    - Elle possède 2 guetters: un pour le nombre de message envoyer et un pour le boolean qui indique la fin de la diffusion.

  - **Serveur**
      - Cette classe créer un objet serveur qui contient les informations de data et du multicast
      - Sa fontion principale gère la communication client et les messages LAST du client. Pour ce faire on récupère le numéro du dernier message envoyer par la multidiffusion.
      - Elle délègue les messages du type MESS a une fonction auxiliaire Multimess()

  - **Gestionnaire**
      - Ici on communique avec le gestionnaire en gardant la socket ouverte jusqu'à ce que :
        - soit le Gestionnaire nous indique qui ne veut pas ou plus nous garder
        - soit quand la multidiffusion est terminé(cf. boolean du multicast)

## Architecture du projet

```Bash
.
├── Client
│   ├── askLastMsg.c
│   ├── askLastMsg.h
│   ├── askList.c
│   ├── askList.h
│   ├── client.c
│   ├── diff-config.txt
│   ├── Diffuseur.jar
│   ├── ex.txt
│   ├── Makefile
│   ├── parseClient.c
│   ├── parseClient.h
│   ├── receiveMulti.c
│   ├── receiveMulti.h
│   ├── sendFile.c
│   ├── sendFile.h
│   ├── sendMsgToDiff.c
│   ├── sendMsgToDiff.h
│   ├── setSocket.c
│   ├── setSocket.h
│   └── window.c
├── clientclean.sh
├── clientrun.sh
├── clientwin.sh
├── config-client.txt
├── diffclean.sh
├── diffrun.sh
├── Diffuseur
│   ├── Data.java
│   ├── diff-config.txt
│   ├── Gestionnaire.java
│   ├── Main.java
│   ├── Makefile
│   ├── messages.txt
│   ├── Multicast.java
│   ├── README.md
│   └── Serveur.java
├── gestclean.sh
├── Gestionnaire
│   ├── Diffuseur.java
│   ├── GestionnaireMain.java
│   ├── GestionnaireThread.java
│   └── Makefile
├── gestrun.sh
└── Readme.md           # Ce Readme.md 
```
