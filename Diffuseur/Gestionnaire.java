import java.net.*;
import java.io.*;
public class Gestionnaire implements Runnable{
  private Data dat;
  private Multicast mult;

  public Gestionnaire(Data dat, Multicast mult){
    this.dat = dat;
    this.mult = mult;
  }

  public void run(){
        try{
            Socket socket=new Socket(this.dat.getAdrGest(), this.dat.getGest());
            PrintWriter pw=new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            pw.print("REGI "+this.dat.getName()+" "+this.dat.getAdress()+" "+this.dat.getEnvoie()+" "+this.dat.getIpLoc()+" "+this.dat.getRecoit()+"\r\n");
            pw.flush();
            BufferedReader br=new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String mess=br.readLine();
            if (mess.startsWith("REOK")){
              System.out.println("Message reçu : "+mess);
            }
            boolean b = true;
            if(mess.startsWith("RENO")){
              br.close();
              pw.close();
              if (!(socket.isClosed())) socket.close();
              b=false;
            }
            while(b){
              mess = br.readLine();
              if (mess == null){
                System.out.println("Le gestionnaire ne répond plus...");
                b=false;
                break;
              }
              if (mess.startsWith("RUOK")){
                System.out.println("j'ai reçu du diffuseur le message de vérification : "+mess);
                if (!(this.mult.isEnd())) {
                  pw.print("IMOK\r\n");
                  pw.flush();
                  System.out.println("j'ai répondu présent");
                }
              }
            }
            br.close();
            pw.close();
            if (!(socket.isClosed())) socket.close();
            System.out.println("la socket avec le gestionnaire est fermée");
        }catch(Exception e){
            System.out.println("Gestionnaire Connexion : Failed");
        }
    }
}
