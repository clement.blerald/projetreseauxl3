import java.io.*;
import java.net.*;
import java.util.*;

public class Multicast implements Runnable{

  private int nb_mess;
  private Data dat;
  private boolean end;
  private boolean boucle;

  public Multicast(Data dat){
    this.dat = dat;
    this.nb_mess = 0;
    this.end = false;
    this.boucle = false;
  }

  public int getNbMess(){
    return this.nb_mess;
  }

  public boolean isEnd(){
    return this.end;
  }

  public boolean getBoucle(){
    return this.boucle;
  }


  public void run(){
      try{
        DatagramSocket dso=new DatagramSocket();
        byte[]data;
        int i = 0;
        while (i <= 9999){
          this.nb_mess = i;
          String s="DIFF "+Main.num4(i)+" "+dat.getName()+" "+dat.getMessage(i)+"\r\n";
          data = s.getBytes();
          InetSocketAddress ia=new InetSocketAddress(dat.getAdress(),dat.getEnvoie());
          DatagramPacket paquet=new DatagramPacket(data,data.length,ia);
          if (i==9999){
             i=-1;
             this.boucle = true;
           }
           i++;
          try {
            Thread.sleep(3000);
          } catch (InterruptedException c) {
            c.printStackTrace();
          }
          dso.send(paquet);
        }
      } catch(IOException e) {
        e.printStackTrace();
      }
      this.end = true;
    }
  }
