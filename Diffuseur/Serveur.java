  import java.net.*;
import java.io.*;

public class Serveur implements Runnable{
  private Data dat;
  private Multicast mult;

  public Serveur(Data dat, Multicast mult){
    this.dat = dat;
    this.mult = mult;
  }

  public void MultiMess(String id, String message){
    try{
      DatagramSocket dso=new DatagramSocket();
      byte[]data;
      String s="DIFF "+Main.nom8(id)+" "+Main.mess140(message)+"\r\n";
      data = s.getBytes();
      InetSocketAddress ia=new InetSocketAddress(dat.getAdress(), dat.getEnvoie());
      DatagramPacket paquet=new DatagramPacket(data,data.length,ia);
      dso.send(paquet);
    } catch(IOException e) {
      e.printStackTrace();
    }
  }



    public void run(){
      try {
        ServerSocket server = new ServerSocket(dat.getRecoit());
            while(true){
              Socket socket=server.accept();
              BufferedReader br=new BufferedReader(new InputStreamReader(socket.getInputStream()));
              PrintWriter pw=new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
              System.out.println("début connextion client");
              String mess=br.readLine();
              System.out.println("demande du client : "+mess);
              if (mess.startsWith("LAST")){
                String[] mots = mess.split(" ");
                String aux = mots[1];
                int num = Integer.parseInt(aux);

                int a = this.mult.getNbMess();
                if ((a < num) && (!(this.mult.getBoucle()))) num = a;
                for(int i = num ; i>=0; i--){
                  String s="OLDM "+Main.num4((a-i)%9999)+" "+Main.nom8(dat.getName())+" "+Main.mess140(dat.getMessage(a-i))+"\r\n";
                  pw.print(s);
                  pw.flush();
                }

                pw.print("ENDM\r\n");
                pw.flush();
              }
              if (mess.startsWith("MESS")){
                String[] mots = mess.split(" ");
                String id = mots[1];
                String message = mots[2];
                MultiMess(id, message);
                pw.print("ACKM\r\n");
                pw.flush();
              }
              br.close();
              pw.close();
              if (!(socket.isClosed())) socket.close();
            }
          }catch(Exception e){
            System.out.println(e);
            e.printStackTrace();
          }
    }
}
