import java.util.*;
import java.io.*;

public class Main{

  public static String num4(int i){
    String s = ""+i;
    while (s.length() < 4){
      s = "0"+s;
    }
    return s;
  }

  public static String nom8(String name){
    String nom = name;
    while(nom.length() < 8){
      nom = nom+"#";
    }
    return nom;
  }

  public static String mess140(String message){
    String mess = message;
    while(mess.length() < 140){
      mess = mess+"#";
    }
    return mess;
  }

  public static String adresseForm(String s){
    String[] parts = s.split("\\.");
    for (int i=0; i<parts.length; i++){
      while (parts[i].length()<3){
          parts[i] = "0"+parts[i];
      }
    }
    String adresse =parts[0];
    for (int i=1; i<parts.length; i++){
      adresse = adresse+"."+parts[i];
    }
    return adresse;
  }

  private static ArrayList<String> makeList(String fichier){
    ArrayList<String> s= new ArrayList<>();
    try {

      FileInputStream message = new FileInputStream("messages.txt");
      Scanner sc = new Scanner(message);
      while(sc.hasNextLine()){
        s.add(sc.nextLine());
      }
      sc.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return s;
  }


  private static Data Parse(String fic){
    String name;
    String adress;
    String env;
    String rec;
    String adrgest;
    String gest;
    String iploc;
    ArrayList<String> mess;
    try {
      FileInputStream file = new FileInputStream(fic);
      Scanner scanner = new Scanner(file);
      name = scanner.nextLine();
      adress = scanner.nextLine();
      env = scanner.nextLine();
      rec = scanner.nextLine();
      adrgest = scanner.nextLine();
      gest = scanner.nextLine();
      iploc = scanner.nextLine();
      scanner.close();
      mess = makeList("messages.txt");
      return new Data(name, adress, env, rec, adrgest, gest, iploc, mess);
    }catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }


  public static void main(String[] args){
    //On lit le fichier donnée en argument
    String fic = args[0];
    Data dat = Parse(fic);
    Multicast mult = new Multicast(dat);
    Serveur serv = new Serveur(dat, mult);
    Gestionnaire gest = new Gestionnaire(dat, mult);
    System.out.println(dat);
    Thread Multicast = new Thread(mult);
    Thread Serveur = new Thread(serv);
    Thread Gestionnaire = new Thread(gest);
    Multicast.start();
    Serveur.start();
    Gestionnaire.start();
  }
}
