import java.util.*;

public class Data{
  private String name;
  private String adress;
  private String envoie;
  private String recoit;
  private String adrgest;
  private String gest;
  private String iploc;
  private ArrayList<String> mess;

  public Data(String name, String adress, String envoie, String recoit, String adrgest, String gest, String iploc, ArrayList<String> mess){
    this.name = Main.nom8(name);
    this.adress = Main.adresseForm(adress);
    this.envoie = envoie;
    this.recoit = recoit;
    this.adrgest = Main.adresseForm(adrgest);
    this.gest = gest;
    this.iploc = Main.adresseForm(iploc);
    this.mess = mess;
  }

  public String getName(){
    return this.name;
  }

  public String getAdress(){
    return this.adress;
  }

  public int getEnvoie(){
    return Integer.parseInt(this.envoie);
  }

  public int getRecoit(){
    return Integer.parseInt(this.recoit);
  }

  public String getAdrGest(){
    return this.adrgest;
  }

  public int getGest(){
    return Integer.parseInt(this.gest);
  }

  public String getIpLoc(){
    return this.iploc;
  }

  public ArrayList<String> getMessages(){
    return this.mess;
  }

  public String getMessage(int i){
    //peut être ajouter +1 a i
    int a = i % this.mess.size();
    return Main.mess140(this.mess.get(a));
  }

  public String toString(){
    String s = "Lancement du multicast, du gestionnaire et de la communication client :\n";
    s += "Le nom est : "+this.name+"\n";
    s += "L'adresse de multidiffusion est "+this.adress+"\n";
    s += "Le port qui correspond est le port "+this.envoie+"\n";
    s += "Le client communique par TCP sur le port : "+this.recoit+"\n";
    s += "L'adresse du gestionnaire est "+this.adrgest+"\n";
    s += "Le port qui correspond est le "+this.gest+"\n";
    s += "\n";
    s += "l'adresse IP de ma machine est la suivante : "+this.iploc;
    return s;
  }
}
